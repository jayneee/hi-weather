import { useState, useEffect } from "react";
import UseWeather from "./UseWeather.jsx";
import BarChart from "./chart/BarChart.jsx";
import PieChart from "./chart/PieChart.jsx";
import "./weather.css";

const Weather = () => {
  const [location, setLocation] = useState("taipei");

  const { fetchWeather, tempMaxList, tempMinList, humidity, errorMsg } =
    UseWeather(location);

  const handleLocationChange = (event) => {
    setLocation(event.target.value);
  };

  const handleLocationKeyDown = (event) => {
    if (event?.key === "Enter") {
      fetchWeather();
    }
  };

  useEffect(() => {
    fetchWeather();
  }, []);

  useEffect(() => {}, [tempMaxList, tempMinList]);

  return (
    <div className="weather">
      <div className="location">
        <div>Location</div>
        <div>
          <input
            defaultValue={location}
            onChange={(event) => handleLocationChange(event)}
            onKeyDown={(event) => handleLocationKeyDown(event)}
          />
        </div>
        <div className="btn" onClick={() => fetchWeather()}>
          Search
        </div>
      </div>

      {(() => {
        if (!!errorMsg) {
          return <div className="errorMsg">{errorMsg}</div>;
        } else {
          return (
            <div className="charts">
              <div>
                <h2>Max temperature </h2>
                <BarChart valueUnit="°C" list={tempMaxList}></BarChart>
              </div>
              <div>
                <h2>Min temperature </h2>
                <BarChart valueUnit="°C" list={tempMinList}></BarChart>
              </div>
              <div>
                <h2>Humidity {humidity} %</h2>
                <PieChart percentage={humidity}></PieChart>
              </div>
            </div>
          );
        }
      })()}
    </div>
  );
};

export default Weather;
