import "./barChart.css";
import PropTypes from "prop-types";

const BarChart = ({ valueUnit, list }) => {
  let max = Math.max.apply(
    Math,
    list.map(function (o) {
      return o.value;
    })
  );
  let min = Math.min.apply(
    Math,
    list.map(function (o) {
      return o.value;
    })
  );

  let perTempHeight = 200 / (min < 0 ? max - min : max);

  let bars = [];

  for (let i = 0; i <= list.length - 1; i++) {
    let record = list[i];

    let height =
      (min < 0 ? record.value - min : record.value) * perTempHeight + 40;
    const y = 400 - height;

    let x = 42 * i;
    bars.push(
      <g key={i}>
        <rect width="36" height={height} rx="10" x={x} y={y}></rect>
        <text
          className="bar-value"
          width="36"
          x={x + 17}
          y={y + 20}
          textAnchor="middle"
          fontSize="0.8em"
        >
          {record.value}
        </text>
        <text width="20" y={y - 100} x={x + 16} dy=".35em" writingMode="tb">
          {record.name}
        </text>
      </g>
    );
  }

  return (
    <div>
      <svg height="400" width={list.length * 42}>
        <text fontSize="1em" x="20" y="20">
          unit: {valueUnit}
        </text>
        {bars}
      </svg>
    </div>
  );
};

BarChart.propTypes = {
  valueUnit: PropTypes.string,
  list: PropTypes.array.isRequired,
};

export default BarChart;
