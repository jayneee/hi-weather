const PieChart = ({ percentage }) => {
  let da = (percentage / 100) * 31.4;

  return (
    <div>
      <svg height="120" width="120" viewBox="0 0 20 20">
        <circle r="5" cx="10" cy="10" stroke="#c2dbef" strokeWidth="10" />
        <circle
          r="5"
          cx="10"
          cy="10"
          fill="transparent"
          stroke="#454f79"
          strokeWidth="10"
          strokeOpacity=".8"
          strokeDasharray={`${da} 31.4`}
          transform="rotate(-90) translate(-20)"
        />
      </svg>
    </div>
  );
};

export default PieChart;
