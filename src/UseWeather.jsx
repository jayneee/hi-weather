import { useState } from "react";
import axios from "axios";

const UseWeather = (location) => {
  const [tempMaxList, setTempMaxList] = useState([]);
  const [tempMinList, setTempMinList] = useState([]);
  const [humidity, setHumidity] = useState([]);
  const [errorMsg, setErrorMsg] = useState();

  const fetchData = async (url) => {
    return axios
      .get(url)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        return error;
      });
  };

  const fetchWeather = async () => {
    if (!location) {
      return;
    }
    const url = `https://api.openweathermap.org/data/2.5/forecast?q=${location}&units=metric&appid=${process.env.REACT_APP_OPEN_WEATHER_API_KEY}`;

    try {
      const result = await fetchData(url);

      if (result?.status === 200) {
        const data = dataWrapper(result.data);

        setTempMaxList((tempMaxList) => data.tempMaxList);
        setTempMinList((tempMinList) => data.tempMinList);
        setHumidity(data.humidity);
        setErrorMsg(null);
      } else {
        if (!!result?.response?.data?.message) {
          setErrorMsg(result.response.data.message);
        } else {
          setErrorMsg("Sorry, we got an error!");
        }
      }
    } catch (error) {
      setErrorMsg("Sorry, we got an error!");
    }
  };

  const dataWrapper = (data) => {
    if (data?.cod !== "200") {
      return;
    }
    const dt = new Date();
    dt.setDate(dt.getDate() + 4);
    const maxDt = dt.getTime();

    const list = data.list;

    const records = [];
    for (let i = 0; i <= list.length - 1; i++) {
      if (list[i].dt * 1000 < maxDt) {
        const name = list[i].dt_txt.split(" ")[0];
        const max = Math.round(list[i].main.temp_max * 10) / 10;
        const min = Math.round(list[i].main.temp_min * 10) / 10;

        const lastRecord = records[records.length - 1];

        if (lastRecord?.name !== name) {
          records.push({
            name: name,
            max: max,
            min: min,
          });
        } else {
          if (lastRecord.max < max) {
            records[records.length - 1].max = max;
          }

          if (lastRecord.min > min) {
            records[records.length - 1].min = min;
          }
        }
      }
    }
    return {
      tempMaxList: tempMaxWrapper(records),
      tempMinList: tempMinWrapper(records),
      humidity: list[0]?.main?.humidity,
    };
  };

  const tempMaxWrapper = (list) => {
    let records = [];
    for (let i = 0; i <= list.length - 1; i++) {
      records.push({
        name: list[i].name,
        value: list[i].max,
      });
    }

    return records;
  };

  const tempMinWrapper = (list) => {
    let records = [];
    for (let i = 0; i <= list.length - 1; i++) {
      records.push({
        name: list[i].name,
        value: list[i].min,
      });
    }

    return records;
  };

  return { fetchWeather, tempMaxList, tempMinList, humidity, errorMsg };
};

export default UseWeather;
